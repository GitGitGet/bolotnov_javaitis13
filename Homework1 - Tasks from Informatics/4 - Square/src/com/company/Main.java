package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        int result = 0;

        if (a > b) {
            boolean flag = true;

            while (flag) {
                result = result + a / b;
                //System.out.println(result);
                int balance = a % b;
                a = b;
                b = balance;
                if (balance == 0) {
                    flag = false;
                }

            }

        }
        if (a < b) {
            boolean flag = true;

            while (flag) {
                result = result + b / a;
                //System.out.println(result);
                int balance = b % a;
                b = a;
                a = balance;
                if (balance == 0) {
                    flag = false;
                }

            }
        }
        else if (a == b) result = 1;

        System.out.println(result);
    }
}
