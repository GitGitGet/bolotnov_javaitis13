package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int a = scanner.nextInt();
        int b = scanner.nextInt();

	    boolean flag = true;
        int nod = 0;

        while (flag) {

            int balance = b % a;

            if (balance == 0) {
                nod = a;
                flag = false;
            }

            b = a;
            a = balance;
        }
        System.out.println(nod);
    }
}
