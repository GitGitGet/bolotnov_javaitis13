package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int a = 0;
        int b = 0;
        int k = n % 2;                //Если n четное, то k = 0; иначе 1

        if (k == 0) {
            a = n / 2 - 1;
            b = n / 2 + 1;
        } else {
            a = n / 2;
            b = n / 2 + 1;
        }

        //Ищем НОД а и b

        boolean flag2 = true;

        while (flag2) {
        boolean flag = true;
        int nod = 0;
        int x = a;
        int y = b;

        while (flag) {

            int balance = y % x;

            if (balance == 0) {
                nod = x;
                flag = false;
            }

            y = x;
            x = balance;
        }

        //если НОД > 1 то а - 1, b + 1

        if (nod > 1) {
            a = a - 1;
            b = b + 1;
        }
        else flag2 = false;
    }
        System.out.println(a + " " + b);

    }
}
