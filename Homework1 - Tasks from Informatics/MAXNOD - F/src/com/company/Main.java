package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int minNOd = 1;

        for (int i = 2; i < Math.sqrt(n); i++) {
            //System.out.println(x + " x " + y + " y " + x % y);
            if (n % i == 0) {
                minNOd = i;
                break;
            }
            if (minNOd > 1) {
                break;
            }

        }
        if (minNOd > 1) {
            System.out.println(n / minNOd + " " + (n - n / minNOd));
        }
        else {
            System.out.println(1 + " " + (n - 1));
        }
    }
}
