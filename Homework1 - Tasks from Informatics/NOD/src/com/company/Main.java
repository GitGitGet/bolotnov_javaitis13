package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        //Вводим 2 числа

        int a = scanner.nextInt();
        int b = scanner.nextInt();

        int c = 1;
        int d = 1;

        int secondBalance = 0;

        //выясняем остаток от деления а на b

        if(a == 0 || b == 0) {
            System.exit(255);
        }

        if (a > b) {
            int balance = a % b; //140 / 96 (остаток 44)
            int x = b;
            //System.out.println(balance + " balance");
            while (balance != 0) {
                secondBalance = x % balance;  //96 / 44 = (остаток 8)

                //System.out.println(secondBalance + "   secondBalance");

                x = balance;

                //System.out.println(x + "    b");

                balance = secondBalance;

                //System.out.println(balance + "   balance");

            }
            c = a / x;
            d = b / x;

        }

        if (a < b) {
            int balance = b % a; //140 / 96 (остаток 44)
            int x = a;
            //System.out.println(balance + " balance");
            while (balance != 0) {
                secondBalance = x % balance;  //96 / 44 = (остаток 8)

                //System.out.println(secondBalance + "   secondBalance");

                x = balance;

                //System.out.println(x + "    b");

                balance = secondBalance;

                // System.out.println(balance + "   balance");

            }
            c = a / x;
            d = b / x;

        }

        System.out.println(c + " " + d);

    }
}