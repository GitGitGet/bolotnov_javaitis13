package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        boolean flag = true;
        int nod = 1;
        while (flag) {

            int balance = y % x;

            if (balance == 0) {
                nod = x;
                flag = false;
            }

            y = x;
            x = balance;
        }
        System.out.println(nod);
    }
}
