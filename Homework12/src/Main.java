import add.AddElements;
import contain.Contain;
import getFirst.GetFirstElement;
import remove.RemoveElements;
import timeCounter.TimeCounter;
import timeCounter.TimeCounterImpl;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        ArrayList arrayList = new ArrayList();
        LinkedList linkedList = new LinkedList();
        int[] array = new int[1000000];
        HashSet<Integer> hashList = new HashSet<>();

        TimeCounter timeCounter = new TimeCounterImpl();

        AddElements adder = new AddElements(arrayList, linkedList, array, hashList);
        adder.add();
        timeCounter.countTime(adder.getTimePointer());

        RemoveElements remover = new RemoveElements(arrayList, linkedList, array, hashList);
        remover.remove();
        timeCounter.countTime(remover.getTimePointer());

        GetFirstElement getFirstElement = new GetFirstElement(arrayList, linkedList, array, hashList);
        getFirstElement.getFirst();
        timeCounter.countTime(getFirstElement.getTimePointer());

        Contain contain = new Contain(arrayList, linkedList, array, hashList);
        contain.isContain();
        timeCounter.countTime(contain.getTimePointer());
    }
}
