package add;

import java.util.HashSet;
import java.util.List;

public interface Add {
    void add(List list);
    void add(int[] array);
    void add(HashSet hashSet);
}
