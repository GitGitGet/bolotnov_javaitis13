package add;

import timeCounter.FunctionTimeValue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;

public class AddElements {
    private ArrayList arrayList;
    private LinkedList linkedList;
    private int[] array;
    private HashSet map;
    private long[] timePointer;

    public AddElements(ArrayList arrayList, LinkedList linkedList, int[] array, HashSet map) {
        this.arrayList = arrayList;
        this.linkedList = linkedList;
        this.array = array;
        this.map = map;
    }

    Adder adder = new Adder();

    long a, b, c, d, e = 0;

    public void add() {
        a = System.currentTimeMillis();
        adder.add(arrayList);
        b = System.currentTimeMillis();
        adder.add(linkedList);
        c = System.currentTimeMillis();
        adder.add(array);
        d = System.currentTimeMillis();
        adder.add(map);
        e = System.currentTimeMillis();
    }

    public FunctionTimeValue getTimePointer() {
        String name = "Adder";
        long[] timePointer = new long[]{a, b, c, d, e};
        return new FunctionTimeValue(name, timePointer);
    }
}
