package add;

import java.util.HashSet;
import java.util.List;
import java.util.Random;

public class Adder implements Add {
    private static final int ELEMENT_TO_ADD = 1000000;
    Random random = new Random();
    @Override
    public void add(List list) {
        for (int i = 0; i < ELEMENT_TO_ADD; i++) {
            list.add(random.nextInt(1000000));
        }
    }
    public void add(int[] array) {
        for (int i = 0; i < ELEMENT_TO_ADD; i++) {
            array[i] = random.nextInt(1000000);
        }
    }
    public void add(HashSet h) {
        for (int i = 0; i < ELEMENT_TO_ADD; i++) {
            h.add(random.nextInt(1000000));
        }
    }


}
