package contain;

import timeCounter.FunctionTimeValue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;

public class Contain {
    private ArrayList arrayList;
    private LinkedList linkedList;
    private int[] array;
    private HashSet map;
    private long[] timePointer;

    public Contain(ArrayList arrayList, LinkedList linkedList, int[] array, HashSet map) {
        this.arrayList = arrayList;
        this.linkedList = linkedList;
        this.array = array;
        this.map = map;
    }

    ContainImpl contain = new ContainImpl();

    long a, b, c, d, e = 0;

    public void isContain() {
        a = System.currentTimeMillis();
        contain.isContain(arrayList);
        b = System.currentTimeMillis();
        contain.isContain(linkedList);
        c = System.currentTimeMillis();
        contain.isContain(array);
        d = System.currentTimeMillis();
        contain.isContain(map);
        e = System.currentTimeMillis();
    }

    public FunctionTimeValue getTimePointer() {
        String name = "Contain";
        long[] timePointer = new long[]{a, b, c, d, e};
        return new FunctionTimeValue(name, timePointer);
    }
}
