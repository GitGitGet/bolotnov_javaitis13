package contain;

import java.util.HashSet;
import java.util.List;
import java.util.Random;

public class ContainImpl implements IsContain{
    private static final int ELEMENT_TO_CHECK = 1000;
    Random random = new Random();
    boolean containElement;
    int c = 0;
    @Override
    public void isContain(List list) {
        for (int i = 0; i < ELEMENT_TO_CHECK; i++) {
            c = random.nextInt(list.size());
             containElement = list.contains(c);
        }
    }
    public void isContain(int[] array) {
        for (int i = 0; i < ELEMENT_TO_CHECK; i++) {
            c = random.nextInt(array.length);
            for (int j = 0; j < array.length; j++) {
                if (array[i] == c) {
                    break;
                }
            }
        }
    }
    public void isContain(HashSet hashSet) {
        for (int i = 0; i < ELEMENT_TO_CHECK; i++) {
            c = random.nextInt(hashSet.size());
            if (hashSet.contains(c)) {
                break;
            }
        }
    }
}
