package contain;

import java.util.HashSet;
import java.util.List;

public interface IsContain {
    void isContain(List list);
    void isContain(int[] array);
    void isContain(HashSet hashSet);
}
