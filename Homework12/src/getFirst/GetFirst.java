package getFirst;

import java.util.HashSet;
import java.util.List;

public interface GetFirst {
    void getFirst(List list);
    void getFirst(int[] array);
    void getFirst(HashSet h);
}
