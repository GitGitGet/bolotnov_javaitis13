package getFirst;

import timeCounter.FunctionTimeValue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;

public class GetFirstElement {

    private ArrayList arrayList;
    private LinkedList linkedList;
    private int[] array;
    private HashSet map;
    private long[] timePointer;


    GetterFirst getterFirst = new GetterFirst();

    public GetFirstElement(ArrayList arrayList, LinkedList linkedList, int[] array, HashSet<Integer> hashList) {
        this.arrayList = arrayList;
        this.linkedList = linkedList;
        this.array = array;
        this.map = hashList;
    }

    long a, b, c, d, e = 0;

    public void getFirst() {
        a = System.currentTimeMillis();
        getterFirst.getFirst(arrayList);
        b = System.currentTimeMillis();
        getterFirst.getFirst(linkedList);
        c = System.currentTimeMillis();
        getterFirst.getFirst(array);
        d = System.currentTimeMillis();
        getterFirst.getFirst(map);
        e = System.currentTimeMillis();
    }

    public FunctionTimeValue getTimePointer() {
        String name = "Get First";
        long[] timePointer = new long[]{a, b, c, d, e};
        return new FunctionTimeValue(name, timePointer);
    }
}
