package getFirst;

import java.util.*;

public class GetterFirst implements GetFirst {

    private static final int COUNT_ELEMENTS_TO_GET = 1000;
    private Random random;

    public GetterFirst() {
        this.random = new Random();
    }

    @Override
    public void getFirst(List list) {
        for (int i = 0; i < COUNT_ELEMENTS_TO_GET; i++) {
            int num = random.nextInt(1000000);
            for (int j = 0; j < list.size(); j++) {
                if (list.get(j).equals(num)) {
                    break;
                }
                break;
            }
        }
    }
    @Override
    public void getFirst(int[] array) {
        for (int i = 0; i < COUNT_ELEMENTS_TO_GET; i++) {
            int num = random.nextInt(1000000);
            for (int j = 0; j < array.length; j++) {
                if (array[i] == num) {
                    break;
                }
            }
        }
    }
    public void getFirst(HashSet h) {
        for (int i = 0; i < COUNT_ELEMENTS_TO_GET; i++) {
            int num = random.nextInt(1000000);
            h.contains(num);
        }
    }
}
