package remove;

import java.util.HashSet;
import java.util.List;

public interface Remove {
    void remove(List list);
    void remove(int[] array);
    void remove(HashSet hashSet);
}
