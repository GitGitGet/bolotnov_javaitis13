package remove;

import timeCounter.FunctionTimeValue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;

public class RemoveElements {
    private ArrayList arrayList;
    private LinkedList linkedList;
    private int[] array;
    private HashSet map;
    private long[] timePointer;

    public RemoveElements(ArrayList arrayList, LinkedList linkedList, int[] array, HashSet map) {
        this.arrayList = arrayList;
        this.linkedList = linkedList;
        this.array = array;
        this.map = map;
    }

    Remover remover = new Remover();

    long a, b, c, d, e = 0;

    public void remove() {
        a = System.currentTimeMillis();
        remover.remove(arrayList);
        b = System.currentTimeMillis();
        remover.remove(linkedList);
        c = System.currentTimeMillis();
        remover.remove(array);
        d = System.currentTimeMillis();
        remover.remove(map);
        e = System.currentTimeMillis();
    }

    public FunctionTimeValue getTimePointer() {
        String name = "Remover";
        long[] timePointer = new long[]{a, b, c, d, e};
        return new FunctionTimeValue(name, timePointer);
    }
}
