package remove;

import java.util.HashSet;
import java.util.List;
import java.util.Random;

public class Remover implements Remove{
    private static final int ELEMENT_TO_REMOVE = 100;
    @Override
    public void remove(List list) {
        Random random = new Random();
        for (int i = 0; i < ELEMENT_TO_REMOVE; i++) {
            list.remove(random.nextInt(list.size() - 1));
        }
    }
    public void remove(int[] array) {
        Random random = new Random();
        for (int i = 0; i < ELEMENT_TO_REMOVE; i++) {
            int[] newArray = new int[array.length - 1];
            int index = random.nextInt(array.length - 1);
            if (index == 0) {
                for (int j = 1; j < array.length; j++) {
                    newArray[j - 1] = array[j];
                }
            } else if (index == array.length - 1) {
                for (int j = 0; j < array.length - 1; j++) {
                    newArray[j] = array[j];
                }
            } else {
                for (int j = 0; j < index; j++) {
                    newArray[j] = newArray[j];
                }
                for (int j = index; j < array.length - 1; j++) {
                    newArray[index] = array[index + 1];
                }
            }
            array = newArray;
        }
    }
    public void remove(HashSet h) {
        Random random = new Random();
        for (int i = 0; i < ELEMENT_TO_REMOVE; i++) {
            h.remove(random.nextInt(h.size()));
        }
    }
}
