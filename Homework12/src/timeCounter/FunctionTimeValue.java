package timeCounter;

public class FunctionTimeValue {
    private String name;
    private long[] arrayPoints;

    private String[] lists = {"ArrayList", "LinkedList", "Integer Array", "HashSet"};

    public String[] getLists() {
        return lists;
    }

    public void setLists(String[] lists) {
        this.lists = lists;
    }

    public FunctionTimeValue(String name, long[] arrayPoints) {
        this.name = name;
        this.arrayPoints = arrayPoints;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long[] getArrayPoints() {
        return arrayPoints;
    }

    public void setArrayPoints(long[] arrayPoints) {
        this.arrayPoints = arrayPoints;
    }
}
