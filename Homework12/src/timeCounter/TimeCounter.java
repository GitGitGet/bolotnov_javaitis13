package timeCounter;

public interface TimeCounter {
    void countTime(FunctionTimeValue functionTimeValue);
}
