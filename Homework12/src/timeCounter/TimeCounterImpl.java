package timeCounter;

public class TimeCounterImpl implements TimeCounter{
    @Override
    public void countTime(FunctionTimeValue functionTimeValue) {
        long[] timePoints = functionTimeValue.getArrayPoints();
        int before = 0;
        System.out.println(functionTimeValue.getName());
        String[] lists = functionTimeValue.getLists();
        for (int i = 1; i < timePoints.length; i++) {
            System.out.println("time for " + lists[i - 1] + ": " + (timePoints[i] - timePoints[before]));
            before++;
        }
        System.out.println("----------------------------");
    }
}
