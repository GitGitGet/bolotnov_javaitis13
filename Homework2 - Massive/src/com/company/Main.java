package com.company;

import java.util.Arrays;
import java.util.Arrays;

public class Main {

    public static int sumEvenPositiveNumbers(int[] array){
        int result = 0;
        for (int value : array) {
            if (value > 0 && value % 2 == 0) {
                result += value;
            }
        }
        return result;

    }

    public static int maxValueOfEvenIndex(int[] array){
        int maxNumber = array[0];
        for(int i = 0; i < array.length; i = i + 2) {
            if (array[i] > maxNumber){
                maxNumber = array[i];
            }
        }
        return maxNumber;

    }

    public static int sumOfElement(int[] array){
        int result = 0;
        for (int value: array)
             {
            result+=value;
        }
        return result;
    }

    public static double arithmeticAverage(int[] array){
        int numberOfElements = array.length;
        int sum = sumOfElement(array);
        double arithmeticAverage;
        arithmeticAverage = (double) sum / numberOfElements;

        return arithmeticAverage;

    }

    public static String elementsThanLessArithmeticAverage(int[] array, double arithmeticAverage) {

        int[] tempArray = new int[array.length];

        arithmeticAverage = arithmeticAverage(array);
        int j = 0;
        for (int value : array) {
            if (value < arithmeticAverage) {
                tempArray[j] = value;
                j++;
            }
        }
        int[] elementsThanLessArithmeticAverage = new int[j];

        for (int i = 0; i < j; i++) {
            elementsThanLessArithmeticAverage[i] = tempArray[i];
        }
        return Arrays.toString(elementsThanLessArithmeticAverage);
    }

    public static String twoMinNumbers(int[] array){
        int min = array[0];
        int min2 = array[1];
        for (int i = 2; i < array.length; i++) {
            if (array[i] < min) {
                min = array[i];
                //System.out.println(min + " min");
            }
            else if (array[i] <= min2) {
                min2 = array[i];
                //System.out.println(min2 + " min2");
            }
        }
        return (min + ", " + min2);
    }

    public static int[] compressArray(int[] array, int a, int b) {

        int[] result = array;
        int tail = array.length - b + a;
        boolean flag = true;
        while (flag){
            if (b >= array.length){
                flag = false;
                break;
            }
            result[a] = array[b];
            a ++;
            b ++;

        }
        for (int i = tail; i < array.length; i++) {
            result[i] = 0;
        }

        return result;
    }

    public static int sumOfRangeModule(int[] array, int a, int b){

        int sum = 0;
        for (int i = a; i <= b; i++) {
            sum += Math.abs(array[i]);
            //System.out.println(sum);
        }
        return sum;
    }

    public static int sumOfModuleAfterFirstNegative(int[] array){
        int begiOfRange = 0;
        int endOfRange = array.length - 1;
        for (int i= 0; i < array.length; i++) {
            if (array[i] < 0) {
                begiOfRange = i + 1;
                break;
            }
        }
        return sumOfRangeModule(array, begiOfRange, endOfRange);
    }

    public static int indexOfMinModule (int[] array) {
        int index = 0;
        for (int i = 1; i < array.length; i++) {
            if (Math.abs(array[i]) < Math.abs(array[index])){
                index = i;
            }
        }
        return index;
    }

    public static int sumOfAllNumber(int[] array) {

        int result = 0;

        for (int i = 0; i < array.length; i ++) {

            int tempValue = Math.abs(array[i]);
            if (tempValue / 10 != 0) {

                while (tempValue != 0) {
                    result += tempValue % 10;
                    tempValue = tempValue / 10;
                    //System.out.println(result + " 1");
                }
            }
            else {
                result += tempValue;
                //System.out.println(result + " 2");
            }
        }

        return result;
    }
    public static int indexOfMin (int[] array) {
        int min = array[0];
        int index = 0;
        for (int i = 1; i < array.length; i++) {
            if (array[i] < min){
                min = array[i];
                index = i;
            }
        }
        //System.out.println(min);
        return index;
    }
    public static int indexOfMax (int[] array) {
        int max = array[0];
        int index = 0;
        for (int i = 1; i < array.length; i++) {
            if (array[i] > max){
                max = array[i];
                index = i;
            }
        }
        //System.out.println(max);
        return index;
    }

    public static int sumBetweenMinMax (int[] array, int min, int max) {
        int result = 0;
        if (min < max) {
            for (int i = min + 1; i < max; i++) {
                result += array[i];
            }
            return result;
        }
        else {
            for (int i = max + 1; i < min; i ++){
                result +=array[i];
            }
            return result;
        }
    }

    public static double arithmeticAverageOfPositiveElements(int[] array){
        int sum = 0;
        double result = 0;
        int count = 0;
        for (int i = 0; i <array.length; i++){
            if (array[i] >0) {
                sum += array[i];
                count++;
            }
        }
        System.out.println(sum);
        result = (double)(sum / count);
        return result;
    }

    public static String maxOfNegative(int[] array) {
        int maxNegative = array[0];
        int index = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] < maxNegative) {
                index = i;
                maxNegative = array[i];
            }
        }
        String string = index + " - index, " + maxNegative + " - Max Negative.";

        return string;
    }

    public static String mostFrequentElement(int[] array) {

        int mostFrequentElement = 0;
        int frq = 1;

        for (int i = 0; i < array.length; i++) {
            int count = 0;
            int c = array[i];
            for (int j = i; j < array.length; j++) {
                if (array[j] == c) {
                    count++;
                }
            }
            if (count > frq) {
                frq = count;
                mostFrequentElement = c;
            }
            //System.out.println(count + " " + frq);
        }
        if (frq == 1) {
            return "All elements are origin";
        }
        else {
            return mostFrequentElement + "";
        }

    }

    public static String swapMinMax(int[] array){
        int minIndex = indexOfMin(array);
        int maxIndex = indexOfMax(array);
        //System.out.println(minIndex + " " + maxIndex);
        int temp = array[maxIndex];
        array[maxIndex] = array[minIndex];
        array[minIndex] = temp;

        return Arrays.toString(array);
    }

    public static String turnArray(int[] array, int step) {

        int[] resultArray = new int[array.length * 2];

        if (step > 0) {

        }
    }


    public static void main(String[] args) {

        int[] array = {1, 45, 56, -23, 1, 78, 34, -12, 67, 1, 245, 7, -10};
        System.out.println(indexOfMinModule(array) + " -------- Index Of Min Module Element");


        System.out.println();
        //Print initial array
        System.out.println(Arrays.toString(array) + " ----- Initial Array");

        //Сумма четных положительных чисел в массиве

        System.out.println(sumEvenPositiveNumbers(array) + " ------------------------------ Sum of Even Values");

        //Максимальный элемент с четным индексом

        System.out.println(maxValueOfEvenIndex(array) + " ------------------------------ Max Element with Even Index");

        //Печатаем среднее арифметическое

        System.out.println(arithmeticAverage(array) + "  ------------- Arithmetic Averrage");

        //Print Array with Elements Less than Arithmetic Average

        System.out.println(elementsThanLessArithmeticAverage(array, arithmeticAverage(array)) + " -- Elements Less than Arithmetic Average");

        //Print two min elements

        System.out.println( twoMinNumbers(array) + " --------------------------------------------------- Two Min Numbers");

        int a = 3;
        int b = 5;

        System.out.println(Arrays.toString(compressArray(array, a, b)) + " ------------------ Compress Array");

        System.out.println(sumOfModuleAfterFirstNegative(array) + " -------------------------------------------- Sum Module");

        System.out.println(indexOfMinModule(array) + " ------------------------------------------------ Index Of Min Module Element");

        System.out.println(sumOfAllNumber(array) + " ------------------------------------------------Sum of All Number");

        System.out.println(sumBetweenMinMax(array, indexOfMin(array), indexOfMax(array)) + " --- Sum between min max");

        System.out.println(arithmeticAverageOfPositiveElements(array) + " ------ arithmetic average of positive");

        System.out.println(maxOfNegative(array) + " ----- Max of Negative");

        System.out.println(mostFrequentElement(array) + " -------- Most Frequent Element");

        System.out.println(swapMinMax(array) + " ------ Swap the min and max elements");

        int step = 3;

        System.out.println(turnArray(array, step) + " -------   Turn Array");
        System.out.println("Hi");
    }
}
