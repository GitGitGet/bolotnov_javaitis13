import com.oracle.xmlns.internal.webservices.jaxws_databinding.SoapBindingParameterStyle;

import java.util.Arrays;

public class ActionSwitcher {


    public void actionSwitch(Sequence[] sequences, int numOfAction) {

        Input input = new Input();
        SequenceModifier sequenceModifier = new SequenceModifier();
        PrintSequense printSequense = new PrintSequense();

        switch (numOfAction) {
            case 1:
                System.out.println("Now I'll add the new Number to the Sequence");
                int actionIndex = input.getRowIndexForAction();
                Input addNewElement = new Input();
                int newElement = addNewElement.getNewElement();
                sequenceModifier.addNewElement(sequences, actionIndex, newElement);
                System.out.println("The Element " + newElement + " added to Sequence");
                printSequense.printActionSequence(sequences, actionIndex);
                break;
            case 2:
                System.out.println("Now I will remove element from row you selected: ");
                int[] actionIndex2 = input.getTwoIndexForAction();
                sequenceModifier.removeNumByIndex(sequences, actionIndex2[0], actionIndex2[1]);
                System.out.println("The Element removed from sequence");
                printSequense.printActionSequence(sequences, actionIndex2[0]);
                break;
            case 3:
                System.out.println("Remove a Number from the Sequence by value");
                int actionIndex3 = input.getRowIndexForAction();
                Input inputValue = new Input();
                int[] resultSeq3 = sequences[actionIndex3].getSequence();
                int value = inputValue.getValue();
                sequenceModifier.removeElementsByValue(sequences, actionIndex3, value);
                System.out.println("The Value " + value + " was removed from Sequence");
                printSequense.printActionSequence(sequences, actionIndex3);
                break;
            case 4:
                System.out.println("Get the Element by the Index");
                int[] actionIndex4 = input.getTwoIndexForAction();
                sequenceModifier.getElementByIndex(sequences, actionIndex4[0], actionIndex4[1]);
                int[] resultSeq4 = sequences[actionIndex4[0]].getSequence();
                System.out.println("The Element is: " + resultSeq4[actionIndex4[1]] + " from Sequence ");
                printSequense.printActionSequence(sequences, actionIndex4[0]);
                break;
            case 5:
                System.out.println("Now I replace Element by Index;");
                int[] actionIndex5 = input.getTwoIndexForAction();
                Input inputNewElement5 = new Input();
                sequenceModifier.replaceNumByIndex(sequences, actionIndex5[0], actionIndex5[1], inputNewElement5.getNewElement());
                System.out.println("The Element replaced by Index " + actionIndex5[0]);
                printSequense.printActionSequence(sequences, actionIndex5[0]);
                break;
            case 6:
                System.out.println("Now I will insert a Number to the Sequence by Index;");
                int[] actionIndex6 = input.getTwoIndexForAction();
                Input inputNewElement = new Input();
                sequenceModifier.insertNumByIndex(sequences, actionIndex6[0], actionIndex6[1], inputNewElement.getNewElement());
                System.out.println("The Number added to sequence " + actionIndex6[0]);
                printSequense.printActionSequence(sequences, actionIndex6[0]);
                break;
        }
    }
}
