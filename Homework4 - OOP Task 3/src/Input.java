import java.util.Random;
import java.util.Scanner;

public class Input {

    private Scanner scanner = new Scanner(System.in);

    public int[] sequenceLimit () {

        Random random = new Random();

        System.out.println("Введите максимальное количество секвенций: ");
        int sequencesMaxCount = scanner.nextInt();

        System.out.println("Введите максимальную длину секвенции");
        int sequencesMaxLength = scanner.nextInt();

        int sequencesCount = random.nextInt(sequencesMaxCount);
        //System.out.println(sequencesCount + " Количество секвенций");
        int[] sequenceLimit = new int[]{sequencesCount, sequencesMaxLength};

        System.out.println("Сейчас я создам " + sequencesCount + " случайных секвенций, каждая длиной не более " + sequencesMaxLength);
        return sequenceLimit;

    }

    public int getNumOfAction() {
        int numOfAction = scanner.nextInt();
        return numOfAction;
    }

    public int[] getTwoIndexForAction() {
        int[] result = new int[2];
        System.out.println("Input the Row Index: ");
        int a = scanner.nextInt();
        System.out.println("Input the Index of the Element");
        int b = scanner.nextInt();
        result[0] = a;
        result[1] = b;
        return result;
    }

    public int getRowIndexForAction() {
        System.out.println("Input the Row Index: ");
        return scanner.nextInt();
    }

    public int getNewElement() {
        System.out.println("Input new Element you want to place: ");
        return scanner.nextInt();
    }

    public int getValue() {
        System.out.println("Input Value you want to delete: ");
        return scanner.nextInt();
    }

}
