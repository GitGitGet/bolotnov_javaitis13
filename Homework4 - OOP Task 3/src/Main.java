
public class Main {

    public static void main(String[] args) {

        Input input = new Input();
        int[] seqLimit = input.sequenceLimit();

        // Count of Sequences
        int sequenceCount = seqLimit[0];

        //Max Length of any Sequences
        int maxSequenceLength = seqLimit[1];

        SequenceGenerator sequenceGenerator = new SequenceGenerator();
        Sequence[] sequences = sequenceGenerator.createSequences(sequenceCount, maxSequenceLength);

        PrintSequense printSequense = new PrintSequense();
        printSequense.printSequense(sequences);

        System.out.println("--------------------------------------");

        System.out.println(

                "Input Number of Action you want to perform: \n\n" +
                "1. Add a Number to the Sequence;\n" +
                "2. Remove a Number from the Sequence by Index;\n" +
                "3. Remove Elements by Value\n" +
                "4. Get the Element by Index\n" +
                "5. Replace Element by Index\n" +
                "6. Insert Num by Index");

        Input inputNumOfAction = new Input();
        int numOfAction = inputNumOfAction.getNumOfAction();

        ActionSwitcher actionSwitcher = new ActionSwitcher();

        actionSwitcher.actionSwitch(sequences, numOfAction);

        //printSequense.printSequense(sequences);
    }
}
