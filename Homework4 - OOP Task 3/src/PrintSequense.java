import java.util.Arrays;

public class PrintSequense {

    public void printSequense(Sequence[] sequences) {

        for (int i = 0; i < sequences.length; i++) {
            Sequence currentSeq = sequences[i];
            System.out.println(currentSeq.getNumber() + "№ " + " Sequense is " + Arrays.toString(currentSeq.getSequence()));
        }
    }

    public void printActionSequence(Sequence[] sequences, int rowIndex) {
        Input input = new Input();
        Sequence currentSeq = sequences[rowIndex];
        System.out.println(currentSeq.getNumber() + "№ " + Arrays.toString(currentSeq.getSequence()));
    }
}
