/**
 * 20.02.2021
 * 16. OOP Task 3 (Home)
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// класс для работы с последовательностью чисел
public class Sequence {

        private int number;
        private int[] sequence;

        public Sequence(int number, int[] sequence) {
                this.number = number;
                this.sequence = sequence;
        }

        public int getNumber() {
                return number;
        }

        public void setNumber(int number) {
                this.number = number;
        }

        public int[] getSequence() {
                return sequence;
        }

        public void setSequence(int[] sequence) {
                this.sequence = sequence;
        }

        public void removeNumByIndex(Sequence[] sequences, int a, int b){

                int[] currentSeq = sequences[a].getSequence();
                int[] newCurrentSeq = new int[currentSeq.length - 1];
                Sequence newSeq = new Sequence(a, newCurrentSeq);
                for (int i = 0; i < b; i++) {
                        newCurrentSeq[i] = currentSeq[i];
                }
                for (int i = b; i < newCurrentSeq.length; i++) {
                        newCurrentSeq[i] = currentSeq[i + 1];
                }

        }

        }