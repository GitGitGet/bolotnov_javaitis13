import java.util.Random;

// генерирует массив случайных последовательностей

public class SequenceGenerator {

    private Random random;

    public SequenceGenerator() {
        this.random = new Random();
    }


    public  int[] createOneSequence(int sequenceMaxLength) {

        int sequenceLength = random.nextInt(sequenceMaxLength);
        int[] sequence = new int[sequenceLength];
        for (int i = 0; i < sequenceLength; i ++) {
            sequence[i] = random.nextInt(100);
        }
        return sequence;
    }
    public Sequence[] createSequences(int sequenseCount, int sequenceMaxLength){
        Sequence[] sequences = new Sequence[sequenseCount];
        for (int i = 0; i < sequences.length; i++) {
            Sequence sequence = new Sequence(i + 1, createOneSequence(sequenceMaxLength));
            sequences[i] = sequence;
        }
        return sequences;
    }
}


