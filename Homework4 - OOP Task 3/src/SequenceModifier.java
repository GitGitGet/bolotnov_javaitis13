public class SequenceModifier {

    public Sequence[] addNewElement(Sequence[] sequences, int a, int newElement) {

        Sequence[] result = sequences;
        int[] currentSeq = sequences[a].getSequence();
        int[] newCurrentSeq = new int[currentSeq.length + 1];
        for (int i = 0; i < currentSeq.length; i++) {
            newCurrentSeq[i] = currentSeq[i];
        }
        newCurrentSeq[currentSeq.length] = newElement;
        Sequence newSeq = new Sequence(a, newCurrentSeq);
        result[a] = newSeq;
        return result;
    }

    public Sequence[] insertNumByIndex(Sequence[] sequences, int a, int b, int newElement){

        Sequence[] result = sequences;
        int[] currentSeq = sequences[a].getSequence();
        int[] newCurrentSeq = new int[currentSeq.length + 1];
        System.out.println(newCurrentSeq.length);
        Sequence newSeq = new Sequence(a, newCurrentSeq);
        for (int i = 0; i < b; i++) {
            newCurrentSeq[i] = currentSeq[i];
        }
        for (int i = b; i < currentSeq.length; i++) {
            newCurrentSeq[i + 1] = currentSeq[i];
        }
        newCurrentSeq[b] = newElement;
        result[a] = newSeq;
        return result;
    }

    public Sequence[] removeNumByIndex(Sequence[] sequences, int a, int b){
        Sequence[] result = sequences;
        int[] currentSeq = sequences[a].getSequence();
        int[] newCurrentSeq = new int[currentSeq.length - 1];
        Sequence newSeq = new Sequence(a, newCurrentSeq);
        for (int i = 0; i < b; i++) {
            newCurrentSeq[i] = currentSeq[i];
        }
        for (int i = b; i < newCurrentSeq.length; i++) {
            newCurrentSeq[i] = currentSeq[i + 1];
        }
        result[a] = newSeq;
        return result;
    }

    public Sequence[] removeElementsByValue(Sequence[] sequences, int rowIndex, int value){
        Sequence[] result = sequences;
        int[] currentSeq = sequences[rowIndex].getSequence();
        System.out.println("123");
        int valueCount = 0;
        for (int i = 0; i < currentSeq.length; i++) {
            if(currentSeq[i] == value) {
                valueCount ++;
            }
        }

        int[] newCurrentSeq = new int[currentSeq.length - valueCount];
        //System.out.println("234");
        Sequence newseq = new Sequence(rowIndex, newCurrentSeq);
        //System.out.println("345");
        int j = 0;
        for (int i = 0; i < currentSeq.length; i++) {
            if (currentSeq[i] != value) {
                newCurrentSeq[j] = currentSeq[i];
                j++;
                //System.out.println("j " + j);
            }
        }
        result[rowIndex] = newseq;
        return result;
    }
    public int getElementByIndex(Sequence[] sequences, int rowIndex, int elementIndex){
        int[] currentSeq = sequences[rowIndex].getSequence();
        return currentSeq[elementIndex];
    }
    public Sequence[] replaceNumByIndex(Sequence[] sequences, int a, int b, int newElement){

        int[] currentSeq = sequences[a].getSequence();
        currentSeq[b] = newElement;
        Sequence newSeq = new Sequence(a, currentSeq);
        sequences[a] = newSeq;

        return sequences;
    }

}
