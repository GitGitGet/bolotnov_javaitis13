package com.company;

import java.util.Arrays;

public class Course {

    private String name;
    private int hours;
    private Student[] students;
    private Tutor tutor;

    @Override
    public String toString() {
        if (students == null) {
            return "Course{" +
                    "name='" + name + '\'' +
                    ", hours=" + hours +
                    ", students=" + Arrays.toString(students) +
                    '}';
        }

        return "Course{" +
                "name='" + name + '\'' +
                ", hours=" + hours +
                ", students=" + students.length +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public Student[] getStudents() {
        return students;
    }

    public void setStudents(Student[] students) {
        this.students = students;
    }

    public Tutor getTutor() {
        return tutor;
    }

    public void setTutor(Tutor tutor) {
        this.tutor = tutor;
    }

    public Course(String name, int hours, Student[] students, Tutor tutor) {
        this.name = name;
        this.hours = hours;
        this.students = students;
        this.tutor = tutor;
    }

    public Course(String name, int hours, Tutor tutor) {
        this(name, hours, null, tutor);
    }

    public Course(String name, int hours) {
        this(name, hours, null, null);
    }

    public String getStudentInfo() {
        if (students == null) return null;
        String result = "";
        for (int i = 0; i < students.length - 1; i++) {
            result += students[i].getFullName() + ", ";
        }
        result += students[students.length - 1].getFullName() + ".";
        return result;
    }

    public int getStudentsQuantity() {
        if (students == null) return 0;
        return students.length;
    }

}
