package com.company;

import java.util.Random;

public class CourseGenerator {

    private static String[] names =

    {
                "Химия",
                "Физика",
                "Математика",
                "Биология",
                "Английский",
                "Русский",
                "География",
                "Право",
                "Обществознание",
                "Геометрия"
    };

    private Random random;

    public CourseGenerator() {
        this.random = new Random();
    }

    public Course[] getCourses(int n){

        if (n > names.length)
            throw new IllegalArgumentException("Can't generate courses. Not enough names.");
        Course[] courses = new Course[n];
        for (int i = 0; i < n; i++) {
            courses[i] = generateCourse(i);
        }
        return courses;
    }

    public Course generateCourse(int i) {

        return new Course(names[i], random.nextInt(150));

    }
}
