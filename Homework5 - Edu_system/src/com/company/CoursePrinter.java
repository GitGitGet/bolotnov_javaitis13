package com.company;


public class CoursePrinter {

    public void printCourses(Course[] courses, int i) {

        switch (i) {
            case 1:
                for (Course course : courses) {
                    printCourseFullInfo(course);
                }
                break;
            case 2:
                System.out.println("Course Students:");
                for (Course course : courses) {
                    printStudents(course);
                }
                break;
        }
    }

    public void printCourseFullInfo(Course course) {
        System.out.println(course.toString());
    }
    public void printStudents(Course course) {
        System.out.println(course.getName() +
                ": Total number of students - " +
                course.getStudentsQuantity() + ". " +
                "Students: " +
                course.getStudentInfo());
    }

    public void printStudentsScores(Course course) {
        Student[] students = course.getStudents();
        for (int i = 0; i < students.length; i++) {
            Student student = students[i];
            System.out.println(student.getFullName() + " " + student.getScores());
        }
        }
    }


