package com.company;

import java.util.Random;

public class CourseStudentAutoMatcher {

    private StudentGenerator studentGenerator;

    private Course[] courses;
    private Student[] students;

    private Random random;

    public CourseStudentAutoMatcher() {
        this.random = new Random();
    }

    public void autoMatch(Course[] courses, Stream[] streams){
        for (int i = 0; i < courses.length; i++) {
            int streamsRandomIndex = random.nextInt(streams.length);
            if (streamsRandomIndex == 0) {
                streamsRandomIndex += 1;
            }
            if (streamsRandomIndex == streams.length) {
                streamsRandomIndex = streams.length - 1;
            }
            Student[] students = streams[streamsRandomIndex].getStudents();
            courses[i].setStudents(students);
            for (int j = 0; j < students.length; j++) {
                students[j].setCourse(courses);
            }
        }

    }
}
