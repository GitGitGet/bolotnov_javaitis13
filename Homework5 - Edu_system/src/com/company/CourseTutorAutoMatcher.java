package com.company;

import java.util.Random;

public class CourseTutorAutoMatcher {

    private Random random;

    public CourseTutorAutoMatcher() {
        this.random = new Random();
    }

    public void autoMatch(Course[] courses, Tutor[] tutors) {
        for (Course course: courses) {
            course.setTutor(tutors[random.nextInt(tutors.length)]);
            if (course.getTutor().getCourses() == null) {
                Course[] courseListToSet = new Course[1];
                courseListToSet[0] = course;
                course.getTutor().setCourses(courseListToSet);
            } else {
              Course[] courseArray = course.getTutor().getCourses();

              boolean wasSet = false;
                for (int i = 0; i < courseArray.length; i++) {
                    if (courseArray[i] == null) {
                        courseArray[i] = course;
                        wasSet = true;
                        break;
                    }
                }
                if (!wasSet) {
                    Course[] extendedCourseList = getExtendedCourseList(courseArray);
                    extendedCourseList[courseArray.length] = course;
                    course.getTutor().setCourses(extendedCourseList);
                }
            }
        }
    }
    private Course[] getExtendedCourseList(Course[] courses){
        Course[] extendedCourse = new Course[courses.length + 1];
        for (int i = 0; i < courses.length; i++) {
            extendedCourse[i] = courses[i];
        }
        return extendedCourse;
    }
}
