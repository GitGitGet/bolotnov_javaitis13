package com.company;

public class Main {

    public static void main(String[] args) {

        TutorGenerator tutorGenerator = new TutorGenerator();
        CourseGenerator courseGenerator = new CourseGenerator();
        StudentGenerator studentGenerator = new StudentGenerator();


        Tutor[] tutors = tutorGenerator.getTutors(10);
        Course[] courses = courseGenerator.getCourses(6);
        Stream[] streams = studentGenerator.getStudentsStream();
        ScoreGenerator scoreGenerator = new ScoreGenerator();

        CoursePrinter coursePrinter = new CoursePrinter();
        TutorPrinter tutorPrinter = new TutorPrinter();
        tutorPrinter.printTutor(tutors, 1);
        coursePrinter.printCourses(courses, 1);
        System.out.println("_________________________________________");

        CourseTutorAutoMatcher courseTutorAutoMatcher = new CourseTutorAutoMatcher();
        courseTutorAutoMatcher.autoMatch(courses, tutors);
        System.out.println("Tutors");
        tutorPrinter.printTutor(tutors, 1);
        System.out.println("Courses");
        coursePrinter.printCourses(courses, 1);


        CourseStudentAutoMatcher courseStudentAutoMatcher = new CourseStudentAutoMatcher();
        courseStudentAutoMatcher.autoMatch(courses, streams);
        System.out.println("Courses + --------");
        coursePrinter.printCourses(courses, 1);


        //Print full name of the course students
        System.out.println("----------------------");
        coursePrinter.printCourses(courses, 2);

        //System.out.println();
        //tutorPrinter.printTutorListByCourses(tutors);

        System.out.println();

        //Print Tutor Rating

        RateTutor rateTutor = new RateTutor(tutors);
        Tutor[] sortTutor = rateTutor.sortTutor(tutors);
        tutorPrinter.printTutorRating(sortTutor);
        System.out.println();

        System.out.println("Student ratings \n");

        scoreGenerator.generateScoreArray(courses);
        StudentPrinter studentPrinter = new StudentPrinter();

        studentPrinter.printStudents(courses);



    }
}
