package com.company;

public class RateTutor {

    private Tutor[] tutors;

    public RateTutor(Tutor[] tutors) {
        this.tutors = tutors;
    }

    public Tutor[] sortTutor(Tutor[] tutors) {
        int newLength = tutors.length;
        for (int i = 0; i < tutors.length; i++) {
            if (tutors[i].getCourses() == null) {
                newLength --;
            }
        }
        Tutor[] newNotNullCourses = new Tutor[newLength];
        int k = 0;
        for (int i = 0; i < tutors.length; i++) {
            if (tutors[i].getCourses() != null) {
                newNotNullCourses[k] = tutors[i];
                k++;
            }
        }
        for (int i = 1; i < newNotNullCourses.length; i++) {

            for (int j = newNotNullCourses.length - 1; j >= i; j--) {
//                if (tutors[j].getCourses() == null || tutors[j - 1].getCourses() == null){
//                    j--;
                if (newNotNullCourses[j].getCourses().length > newNotNullCourses[j - 1].getCourses().length) {
                    Tutor temp = newNotNullCourses[j - 1];
                    newNotNullCourses[j - 1] = newNotNullCourses[j];
                    newNotNullCourses[j] = temp;
                }else if (newNotNullCourses[j].getCourses().length == newNotNullCourses[j - 1].getCourses().length) {
                    int a = newNotNullCourses[j].getTotalHours();
                    int b = newNotNullCourses[j - 1].getTotalHours();
                    if (a > b) {
                        Tutor temp = newNotNullCourses[j - 1];
                        newNotNullCourses[j - 1] = newNotNullCourses[j];
                        newNotNullCourses[j] = temp;
                    }
                }
            }
        }
        return newNotNullCourses;
    }

}
