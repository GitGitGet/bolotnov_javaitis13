package com.company;

import java.util.Random;

public class ScoreGenerator {
    private Random random;

    public ScoreGenerator() {
        this.random = new Random();
    }

    public void generateScoreArray(Course[] courses) {
        for (int i = 0; i < courses.length; i++) {
            Student[] students = courses[i].getStudents();
            for (int j = 0; j < students.length; j++) {
                Student student = students[j];
                student.setScores(generateScores());
            }
        }
    }
    public int generateScores(){
            return  2 + random.nextInt(4);
    }
}
