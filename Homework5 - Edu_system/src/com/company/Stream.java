package com.company;

import java.util.Random;

public class Stream {

    private Student[] students;
    private int numberOfStream;

    public Stream(Student[] students, int numberOfStream) {
        this.students = students;
        this.numberOfStream = numberOfStream;
    }

    public Student[] getStudents() {
        return students;
    }
}
