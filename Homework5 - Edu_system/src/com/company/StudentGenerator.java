package com.company;

import java.util.Random;

public class StudentGenerator {

    private Random random;
    int streamCounter = 0;

    public StudentGenerator() {
        this.random = new Random();
    }

    private static String[] namesList = {
                    "Никита Матвеев",
                    "Евгений Степанов",
                    "Вера Романова",
                    "София Новикова",
                    "София Савина",
                    "Илья Никулин",
                    "Ольга Лебедева",
                    "Алёна Емельянова",
                    "Анна Васильева",
                    "Святослав Зубков",
                    "Руслан Кочетков",
                    "Матвей Григорьев",
                    "Арина Давыдова",
                    "Ярослав Наумов",
                    "Екатерина Софронова",
                    "Кира Петрова",
                    "Вера Ермолова",
                    "Михаил Елисеев",
                    "Павел Смирнов",
                    "Иван Петров",
                    "Иван Усов",
                    "Таисия Федорова",
                    "Тимур Антонов",
                    "Ярослав Фролов",
                    "Полина Мартынова",
                    "Владимир Куликов",
                    "Ангелина Данилова",
                    "Анна Муравьева",
                    "Ангелина Бирюкова",
                    "Анна Денисова",
                    "Илья Богомолов",
                    "Михаил Иванов",
                    "Софья Михайлова"
    };

    private String[] groupNames = {"Economists", "Biologists", "Geologists"};



    public Student getNewStudent(){

        return new Student(namesList[random.nextInt(namesList.length)],
                17 + random.nextInt(5),
                groupNames[random.nextInt(2)], null);
    }

    public Stream[] getStudentsStream() {
        Stream[] streams = new Stream[1 + random.nextInt(10)];
        for (int i = 0; i < streams.length; i++) {
            streams[i] = getNewStream();
        }
        return streams;
    }

    public Stream getNewStream() {
        streamCounter ++;
        return new Stream(generateStudentsList(19), streamCounter);
    }
    public Student[] generateStudentsList(int j) {

        Student[] students = new Student[1 + random.nextInt(j)];

        for (int i = 0; i < students.length; i++) {
            students[i] = getNewStudent();
        }
        return students;
    }
}
