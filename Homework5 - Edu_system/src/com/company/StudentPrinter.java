package com.company;


public class StudentPrinter {

    public void printStudents(Course[] courses) {

        for (int i = 0; i < courses.length; i++) {
            Course course = courses[i];
            System.out.println("Course " + course.getName() + " contains " + course.getStudents().length + " students:");
            System.out.println();
            System.out.println("Score" + " | " + "Student");
            Student[] students = course.getStudents();

            for (int j = 0; j < students.length; j++) {
                Student student = students[j];
                System.out.println(student.getScores() + "     | " + student.getFullName());
            }
            System.out.println();
        }
    }
    public void printRatingStudents(Course[] courses) {

    }
}
