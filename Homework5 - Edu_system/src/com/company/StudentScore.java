package com.company;

public class StudentScore {
    private int score;
    private Student student;
    private Course[] courses;

    public StudentScore(int score, Student student, Course[] courses) {
        this.score = score;
        this.student = student;
        this.courses = courses;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Course[] getCourses() {
        return courses;
    }

    public void setCourses(Course[] courses) {
        this.courses = courses;
    }
}
