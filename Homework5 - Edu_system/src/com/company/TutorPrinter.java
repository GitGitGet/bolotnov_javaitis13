package com.company;

import java.util.Arrays;

public class TutorPrinter {

    private RateTutor rateTutor;


    public void printTutor(Tutor[] tutors, int i) {

        switch (i) {
            case 1:
                for (Tutor tutor : tutors) {
                    printTutorFullInfo(tutor);
                }
                break;
            case 2:

                //Чем больше часов и курсов у преподавателя, тем он круче
                int[] rateTutorArray = new int[tutors.length];
                for (Tutor tutor : tutors) {
                    rateTutorArray[i] = tutor.getCourses().length;
                }
                int tutorIndex = 0;
                int max = rateTutorArray[0];
                for (int j = 0; j < rateTutorArray.length; j++) {
                    if (rateTutorArray[i] > max) {
                        max = rateTutorArray[i];
                        tutorIndex = i;
                    }
                }
                break;
        }
    }

    public void printTutorFullInfo(Tutor tutor) {
        System.out.println(tutor.toString());
        //System.out.println("Courses: " + tutor.getCoursesInfo());
    }

    public void printTutorRating(Tutor[] tutors) {
        System.out.println();
        System.out.println("Tutor rating: ");

        for (int i = 0; i < tutors.length; i++) {
            System.out.println((i + 1) + "№" + " " + tutors[i].getFullName() +
                    " has " + tutors[i].getCourses().length + " courses."+
                    " Total hours " + tutors[i].getTotalHours() +
                    " (" + tutors[i].getCoursesNames() + ")");
        }
    }
    public void printTutorListByCourses(Tutor[] tutors) {
        for (int i = 0; i < tutors.length; i++) {
            if (tutors[i].getCourses() != null) {
                System.out.println((i + 1) + ". " +
                        tutors[i].getFullName() +
                        " has " +
                        tutors[i].getCourses().length +
                        " courses");
            } else {
                System.out.println((i + 1) + ". " + tutors[i].getFullName() + " don't has any courses.");
            }
        }
    }
}
