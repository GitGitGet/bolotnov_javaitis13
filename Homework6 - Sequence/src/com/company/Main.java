package com.company;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) {

        Random random = new Random();

        System.out.println("Введите максимальное количество секвенций: ");
        Scanner scanner = new Scanner(System.in);
        int sequencesMaxCount = scanner.nextInt();

        System.out.println("Введите максимальную длину секвенции");
        int sequencesMaxLength = scanner.nextInt();



        int sequenceCount = random.nextInt(sequencesMaxCount);
        System.out.println(sequenceCount + " Количество секвенций");

        System.out.println("Сейчас я создам " + sequenceCount + " случайных секвенций, каждая будет длиной не более " + sequencesMaxLength);


        for (int i = 0; i < sequenceCount; i++) {
            System.out.println("--------------------------------");

            int sequenceLength = random.nextInt(sequencesMaxLength);
            //System.out.println("Длина конкретной секвенции = " + sequenceLength);
            //System.out.println(sequenceLength);
            int[] sequence = new int[sequenceLength];
            for (int j = 0; j < sequence.length; j++) {
                sequence[j] = random.nextInt(10);
            }
            System.out.println(Arrays.toString(sequence));
        }
    }

}
