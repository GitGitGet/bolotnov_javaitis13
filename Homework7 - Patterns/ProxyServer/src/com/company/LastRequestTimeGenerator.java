package com.company;

import java.util.Random;

public class LastRequestTimeGenerator {
    private Random random;

    public LastRequestTimeGenerator() {
        this.random = new Random();
    }
    public int getTimeLastRequest() {
        return random.nextInt(100);
    }
}
