package com.company;

public class Main {

    public static void main(String[] args) {

        ShowWeather showWeather = new ProxyWeather();

        showWeather.show();
        showWeather.show();
        showWeather.show();
        showWeather.show();
        showWeather.show();
        showWeather.show();
        showWeather.show();
    }
}
