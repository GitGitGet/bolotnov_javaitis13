package com.company;

public class ProxyWeather implements ShowWeather {

    LastRequestTimeGenerator timeGenerator = new LastRequestTimeGenerator();

    Weather weather;

    @Override
    public void show() {
        if (weather == null) {
            weather = new Weather();
            System.out.println("1. The first request for weather, the data received.");
            weather.show();
        }
        if (timeGenerator.getTimeLastRequest() < 50) {
            System.out.println("2. The request is too fast, the data didn't have time to update, the old data is shown.");
            weather.show();
        } else {
            System.out.println("3. The weather data has been updated.");
            weather = new Weather();
            weather.show();
        }
    }
}
