package com.company;


import java.util.Random;

public class Weather implements ShowWeather{

    Random random = new Random();

    String[] winds = {"North", "South", "West", "East"};

    private int degree;
    private String wind;

    public Weather() {
        this.degree = random.nextInt(30);
        this.wind = winds[random.nextInt(3)];
    }

    @Override
    public void show() {
        System.out.println(degree + " degrees, " + wind + " wind.");
    }
}
