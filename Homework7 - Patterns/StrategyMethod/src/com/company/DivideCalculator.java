package com.company;

public class DivideCalculator implements Calculator {

    private int a;
    private int b;

    public DivideCalculator(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public void calculate() {
        System.out.println(a / b);
    }
}