package com.company;

public class Main {

    public static void main(String[] args) {
	    Input input = new Input();
	    String operation = input.getInput();
	    int a = input.getNum();
	    int b = input.getNum();

	    switch (operation) {
			case "+":
				Calculator calculator = new SumCalculator(a, b);
				calculator.calculate();
				break;

			case  "/":
				calculator = new DivideCalculator(a, b);
				calculator.calculate();
				break;
			case "-":
				calculator = new SubtractionCalculator(a, b);
				calculator.calculate();
				break;

			case  "*":
				calculator = new MultiCalculator(a, b);
				calculator.calculate();
				break;
		}
    }
}
