package com.company;

public class MultiCalculator implements Calculator {

    private int a;
    private int b;

    public MultiCalculator(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public void calculate() {
        System.out.println(a * b);
    }
}