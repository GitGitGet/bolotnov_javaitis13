package com.company;

public class SubtractionCalculator implements Calculator {
    private int a;
    private int b;

    public SubtractionCalculator(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public void calculate() {
        System.out.println(a - b);
    }
}
