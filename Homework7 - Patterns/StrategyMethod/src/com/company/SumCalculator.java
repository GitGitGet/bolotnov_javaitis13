package com.company;

public class SumCalculator implements Calculator {

    private int a;
    private int b;

    public SumCalculator(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public void calculate() {
        System.out.println(a + b);
    }
}
