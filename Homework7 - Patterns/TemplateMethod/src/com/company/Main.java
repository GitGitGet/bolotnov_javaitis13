package com.company;

public class Main {

    public static void main(String[] args) {

        Visitors visitors1 = new GoldCard();
        visitors1.showDiscount(100);
        System.out.println("---------------------------");
        Visitors visitor2 = new SilverCard();
        visitor2.showDiscount(100);
        System.out.println("---------------------------");
        Visitors visitor3 = new SimpleCard();
        visitor3.showDiscount(100);
    }
}
