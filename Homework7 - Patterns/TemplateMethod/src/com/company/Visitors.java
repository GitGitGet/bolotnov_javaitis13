package com.company;

abstract class Visitors {

    protected int discount;

    void showDiscount(int price) {

        System.out.println("Insert the card.");
        differ();
        System.out.println("Your discount is " + getDiscount());
        System.out.println("The price for you is " + (price - price * getDiscount()/100));
        System.out.println("Thank you for your purchase!");
    }
    abstract void differ();

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }
}
