package com.company;

public class ArrayList implements List {

    private static final int DEFAULT_SIZE = 10;
    private int[] elements;
    private int count;

    public ArrayList() {
        this.elements = new int[DEFAULT_SIZE];
        this.count = 0;
    }

    @Override
    public int get(int index) {
        return elements[index];
    }

    @Override
    public void removeAt(int index) {
        int[] temp = new int[elements.length - 1];
        for (int i = 0; i < index; i++) {
            temp[i] = elements[i];
        }
        for (int i = index + 1; i < elements.length; i++) {
            temp[i - 1] = elements[i];
        }
        elements = temp;
    }

    public void removeFrom(int index) {
        int[] newArray = new int[index];
        for (int i = 0; i < index; i++) {
            newArray[i] = elements[i];
        }
        elements = newArray;
        count = index;
    }

    @Override
    public int indexOf(int element) {
        int index = -1;
        for (int i = 0; i < count; i++) {
            if (elements[i] == element) {
                index = i;
                break;
            }
        }
        return index;
    }

    @Override
    public int lastIndexOf(int element) {
        int index = -1;
        for (int i = count - 1; i >= 0; i--) {
            if (elements[i] == element) {
                index = i;
            }
        }
        return index;
    }

    @Override
    public boolean containsFromIndex(int element, int index) {
        boolean contain = false;
        for (int i = index; i < count; i++) {
            if (elements[i] == element) {
                contain = true;
                break;
            }
        }
        return contain;
    }

    @Override
    public void add(int element) {
        if (count == elements.length) {
            int[] newArray = new int[elements.length + elements.length/2];
            for (int i = 0; i < count; i++) {
                newArray[i] = elements[i];
            }
            elements = newArray;
        }
        elements[count] = element;
        count++;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public boolean contains(int element) {
        boolean contain = false;
        for (int i = 0; i < count; i++) {
            if (elements[i] == element) {
                contain = true;
                break;
            }
        }
        return contain;
    }

    @Override
    public void removeFirst(int element) {
        for (int i = 0; i < count; i++) {
            if (elements[i] == element) {
                resize(i);
                break;
            }
        }
    }

    @Override
    public void removeLast(int element) {
        int index = lastIndexOf(element);
        resize(index);
    }

    public void resize(int index) {
        int[]temp = new int[count - 1];
        for (int i = index; i < temp.length; i++) {
            temp[i] = elements[i + 1];
        }
        elements = temp;
        count--;
    }

    @Override
    public void removeAll(int element) {
        int check = 0;
        while (check < count && containsFromIndex(element, check)) {
            int positionOfRemovingElement;
            for (int i = check; i < count; i++) {
                if (elements[i] == element) {
                    positionOfRemovingElement = i;
                    check = i;
                    for (int j = positionOfRemovingElement; j < count - 1; j++) {
                        elements[j] = elements[j + 1];
                    }
                    count--;
                    break;
                }
            }
        }
    }
    public class ArrayListIterator implements Iterator {
        private int currentPosition;

        @Override
        public int next() {
            int nextValue = elements[currentPosition];
            currentPosition++;
            return nextValue;
        }

        @Override
        public boolean hasNext() {
            return currentPosition < count;
        }
    }
    @Override
    public Iterator iterator() {
        return new ArrayListIterator();
    }
}
