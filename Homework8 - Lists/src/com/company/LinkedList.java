package com.company;

public class LinkedList implements List{

    public static class Node {
        int value;
        Node next;

        Node(int value) {
            this.value = value;
        }
    }

    private Node first;
    private Node last;
    private int count;

    @Override
    public int get(int index) {
        if (index < count && index > -1) {
            Node current = this.first;
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            return current.value;
        }
        System.err.println("Out of Bounds");
        return -1;
    }

    @Override
    public void removeAt(int index) {
        if (index == 0) {
            first = first.next;
            count--;
            return;
        }
        Node current = first;
        Node temp = current;
        for (int i = 0; i < index; i++) {
            temp = current;
            current = current.next;
        }
        temp.next = current.next;
        count--;
    }

    @Override
    public int indexOf(int element) {
        int index = 0;
        Node current = first;
        boolean flag = false;
        while (!flag && index < count - 1) {
            if (current.value == element) {
                flag = true;
                return index;
            } else {
                current = current.next;
                index++;
            }
        }
        if (!flag) {
            System.err.println("The element isn't in List");
            return -1;
        } else {
            return index;
        }
    }

    @Override
    public int lastIndexOf(int element) {
        if (last.value == element) {
            return count - 1;
        }
        else {
            Node current = first;
            int index = 0;
            int i = 0;
            while (current.next != null) {
                if (current.value == element) {
                    index = i;
                    current = current.next;
                    i++;
                } else {
                    current = current.next;
                    i++;
                }
            }
            return index;
        }
    }

    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            this.first = newNode;
        } else {
            last.next = newNode;
        }
        this.last = newNode;
        this.count ++;
    }

    @Override
    public int size() {
        return count;
    }


    @Override
    public void removeFirst(int element) {

        Node current = first;

        if (current.value == element) {
            first = first.next;
            return;
        }
        while (current.next != null && current.next.value != element) {
            current = current.next;
        }
        if (current.next != null) {
            current.next = current.next.next;
        } else {
            this.last = current;
        }
        count --;
    }

    @Override
    public void removeLast(int element) {
        Node current = first;
        int i = 1;
        boolean find = false;
        if (current.value == element && !containsFromIndex(element, i + 1)) {
            first = current.next;
            count--;
            return;
        }
        if (last.value == element) {
            for (int j = 0; j < count - 2; j++) {
                current = current.next;
            }
            last = current;
            last.next = null;
            count--;
            return;
        }
        while (!find && i < size() - 1){
            int c = current.next.value;
            if (current.next.value != element) {
                current = current.next;
                i++;
            } else {
                if (!containsFromIndex(element, i + 1)) { //current.next.value == element &&  не нужно уже проверять
                    current.next = current.next.next;
                    count--;
                    find = true;
                }
                else {
                    current = current.next;
                    i++;
                }
            }
        }
    }

    @Override
    public void removeAll(int element) {

        Node current = first;
        int i = 0;
        while (i < count) {
            if (current.next != null && current.next.value == element) {
                current.next = current.next.next;
                i++;
                count--;
            } else {
                current = current.next;
                i++;
            }
        }
        if (first.value == element) {
            first = first.next;
            count--;
        }
        if (last.value == element) {
            this.last = current;
            last.next = null;
            count--;
        }
    }

    @Override
    public boolean containsFromIndex(int element, int index) {
        Node current = first.next;
        boolean contains = false;
        for (int i = 0; i < index - 1; i++) {
            current = current.next;
        }

        while (!contains && index < count) {
            if (current.value == element) {
                contains = true;
            } else {
                current = current.next;
                index++;
            }
        }
        return contains;
    }
    @Override
    public boolean contains(int element) {
        Node current = first;
        boolean contains = false;
        while (!contains && current.next != null) {
            if (current.value == element) {
                contains = true;
            } else {
                current = current.next;
            }
        }
        return contains;
    }
    public class LinkedListIterator implements Iterator {

        private LinkedList.Node current;

        public LinkedListIterator() {
            this.current = first;
        }

        public boolean hasNext() {
            return current != null;
        }

        public int next() {
            int value = current.value;
            current = current.next;

            return value;
        }
    }
    @Override
    public Iterator iterator() {
        return new LinkedListIterator();
    }
}
