package com.company;

import com.company.*;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        VideoGenerator videoGenerator = new VideoGenerator();
        UserGenerator userGenerator = new UserGenerator();
        UsersVideosAutoMatcher usersVideosAutoMatcher = new UsersVideosAutoMatcher();
        PrintStat printStat = new PrintStat();
        Money moneyCounter = new Money();

        List<User> users = userGenerator.generateUsersList();
        List<Video> videos = videoGenerator.generateVideoList();

        usersVideosAutoMatcher.userToVideo(users, videos);

        printStat.printVideoList(videos);
        printStat.printUserList(users);
        printStat.printSortingUserNames(users);

        Sorting<Video> videoSortingByLikes = new VideoSorting<>(videos);
        videoSortingByLikes.sorting(videos);
        moneyCounter.count(users);

        Sorting<User> userSortingByMoney = new UserSorting<>(users);
        userSortingByMoney.sorting(users);

        System.out.println(users.size() + " Users, " + videos.size() + " Videos.");

        printStat.printVideoList(videos);
        printStat.printUserList(users);
        printStat.printSortingUserNames(users);
    }
}
