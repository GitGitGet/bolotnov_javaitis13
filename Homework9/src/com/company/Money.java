package com.company;

import java.util.List;

public class Money {

    public void count(List<User> userList) {
        for (int i = 0; i < userList.size(); i++) {
            double result = 0.0;
            List<Video> videoList = userList.get(i).getVideoList();
            if (videoList != null) {
                for (int j = 0; j < videoList.size(); j++) {
                    Video video = videoList.get(j);
                    result += video.getLike() * videoList.size() / (j + 1);
                }
                userList.get(i).setMoney(result);
            }
        }
    }
}
