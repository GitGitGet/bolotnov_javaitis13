package com.company;

import java.util.List;

public class PrintStat {
    private List<User> userList;
    private List<Video> videoList;

    public void printUserList(List<User> users) {
        for (int i = 0; i < users.size(); i++) {
            System.out.println(users.get(i).toString());
        }
    }
    public void printSortingUserNames(List<User> users) {
        for (int i = 0; i < users.size(); i++) {
            System.out.println(i + 1 + ". " + users.get(i).getName() + " - " + "$"  + users.get(i).getMoney());
        }
    }

    public void printVideoList(List<Video> videos) {
        for (int i = 0; i < videos.size(); i++) {
            System.out.println(videos.get(i).toString());
        }
    }
}
