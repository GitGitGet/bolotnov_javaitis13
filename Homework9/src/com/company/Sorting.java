package com.company;

import java.util.List;

public interface Sorting<V> {
    void sorting(List<V> list);
}
