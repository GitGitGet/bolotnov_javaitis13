package com.company;

import java.util.List;

public interface SortingUsers<U> extends Sorting<U>{
    public void sorting(List<U> userList);
}
