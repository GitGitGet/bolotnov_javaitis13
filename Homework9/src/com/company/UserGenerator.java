package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class UserGenerator {
    private Random random;

    public UserGenerator() {
        this.random = new Random();
    }
    String[] authors = {"First", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh", "Eight"};
    public User generateUser() {
        User user = new User();
        String str = authors[random.nextInt(authors.length - 1)];
        user.setName(str);
        List<String> array = new ArrayList<>(Arrays.asList(authors));
        array.remove(str);
        authors = array.toArray(new String[array.size()]);
        user.setMoney(0);
        return user;
    }

    public List<User> generateUsersList() {
        List<User> list = new ArrayList<>();
        int n = 1 + random.nextInt(9);
        for (int i = 0; i < n; i++) {
            list.add(generateUser());
        }
        return list;
    }
}
