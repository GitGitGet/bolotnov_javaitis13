package com.company;

import java.util.Comparator;
import java.util.List;

public class UserSorting<U> implements SortingUsers<U> {

    private List<User> users;

    public UserSorting(List<User> users) {
        this.users = users;
    }

    @Override
    public void sorting(List<U> userList) {
        List<User> userList1 = (List<User>)userList;
        userList1.sort(new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                if (o1.getMoney() < o2.getMoney()) return 1;
                else if (o1.getMoney() == o2.getMoney()) return 0;
                else return -1;
            }
        });
    }
}
