package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class UsersVideosAutoMatcher {

    private Random random;

    public UsersVideosAutoMatcher() {
        this.random = new Random();
    }
    public void userToVideo(List<User> usersList, List<Video> videos) {
        for (int i = 0; i < usersList.size(); i++) {
            List<Video> temp = generateVideoList(videos);
            usersList.get(i).setVideoList(temp);
            for (int j = 0; j < temp.size(); j++) {
                temp.get(j).setAuthor(usersList.get(i));
            }
        }
        videos.clear();
        for (int i = 0; i < usersList.size(); i++) {
            for (int j = 0; j < usersList.get(i).getVideoList().size(); j++) {
                videos.add(usersList.get(i).getVideoList().get(j));
            }
        }
    }
    public List<Video> generateVideoList(List<Video> videos) {
        int n = 1 + random.nextInt(videos.size() / 4);
        List<Video> newVideoList = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            Video newVideo = videos.get(random.nextInt(videos.size()));
            newVideoList.add(newVideo);
            videos.remove(newVideo);
        }
        return newVideoList;
    }
}
