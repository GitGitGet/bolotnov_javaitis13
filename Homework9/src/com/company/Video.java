package com.company;

import java.util.List;
import java.util.Random;

public class Video {
    private String name;
    private User author;
    private int like;
    private int disLike;
    private List<Comments> commentsList;
    private int time;

    private Random random;



    public Video(String name, User author, int like, int disLike, List<Comments> commentsList, int time) {
        this.name = name;
        this.author = author;
        this.like = like;
        this.disLike = disLike;
        this.commentsList = commentsList;
        this.time = time;
    }
    public Video(String name, int like, int disLike, List<Comments> commentsList, int time) {
        this.name = name;
        this.author = null;
        this.like = like;
        this.disLike = disLike;
        this.commentsList = commentsList;
        this.time = time;
    }


    public Video(String name, int time) {
        this(name, null, 0, 0, null, time);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public int getDisLike() {
        return disLike;
    }

    public void setDisLike(int disLike) {
        this.disLike = disLike;
    }

    public List<Comments> getCommentsList() {
        return commentsList;
    }

    public void setCommentsList(List<Comments> commentsList) {
        this.commentsList = commentsList;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public Random getRandom() {
        return random;
    }

    public void setRandom(Random random) {
        this.random = random;
    }

    @Override
    public String toString() {
        if (author != null) {
            return "Video - " + name + ", " +
                    "Author - " + author.getName().toString() + ", " +
                    "like - " + like + ", " +
                    "dislike - " + disLike + ", " +
                    "duration - " + time / 60 + ":" + time % 60;
        } else {
            return "Video - " + name + ", " +
                    "No Author" + ", " +
                    "like - " + like + ", " +
                    "dislike - " + disLike + ", " +
                    "duration - " + time / 60 + ":" + time % 60;
        }
    }
}
