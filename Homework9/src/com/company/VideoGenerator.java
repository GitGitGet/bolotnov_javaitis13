package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class VideoGenerator {
    private final static int MIN_VIDEOS_COUNT = 10;
    private Random random;

    public VideoGenerator() {
        this.random = new Random();
    }
    String[] videoNames = {"Cats", "Dogs", "Humor", "Auto", "Learning", "Java Tutorial", "Funny English", "News"};
    String[] comments = {"good", "bad", "funny", "joyful", "friendship", "boring", "fake", "beautiful", "inspiring"};

    public Video generateVideo() {
        Video video = new Video(
                videoNames[random.nextInt(videoNames.length)],
                null,
                random.nextInt(100),
                random.nextInt(50),
                generateCommentsList(),
                random.nextInt(1000));
        return video;
    }

    public List<Video> generateVideoList() {
        List<Video> list = new ArrayList<>();
        int n = MIN_VIDEOS_COUNT + random.nextInt(20);
        for (int i = 0; i < n; i++) {
            list.add(generateVideo());
        }
        return list;
    }

    public List<Comments> generateCommentsList() {
        List<Comments> commentsList = new ArrayList<>();
        int countOfComments = 1 + random.nextInt(10);
        for (int i = 0; i < countOfComments; i++) {
            Comments comments1 = new Comments();
            comments1.setText(comments[random.nextInt(comments.length)]);
            comments1.setLike(random.nextInt(100));
            comments1.setDisLike(random.nextInt(50));
            comments1.setUserAuthor(null);
            commentsList.add(comments1);
        }
        return commentsList;
    }
}
