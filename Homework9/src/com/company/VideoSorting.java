package com.company;

import java.util.Comparator;
import java.util.List;

public class VideoSorting<V> implements Sorting<V>{

    private List<V> videoList;

    public VideoSorting(List<V> videoList) {
        this.videoList = videoList;
    }

    public void sorting(List<V> videoList) {
        List<Video> videoList1 = (List<Video>) videoList;
        videoList1.sort(new Comparator<Video>() {
            @Override
            public int compare(Video o1, Video o2) {
                if (o1.getLike() == o2.getLike() && o1.getDisLike() == o2.getDisLike()) return 0;
                else if (o1.getLike() < o2.getLike()) return 1;
                else if (o1.getLike() == o2.getLike() && o1.getDisLike() > o2.getDisLike()) return 1;
                else return -1;
            }
        });
    }
}
